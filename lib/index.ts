import { Thunk } from "@valuer/types";

export namespace fn {
	/**
	 * Get negated thunk
	 * @arg thunk Thunk to negate
	 */
	export const not = <T>(thunk: Thunk<T>): Thunk<T> => value =>
		!thunk(value);

	/**
	 * Whether any of the thunks over the input returns `true`
	 * @arg thunks Thunks to pipe together
	 */
	export const any = <T>(...thunks: Thunk<T>[]): Thunk<T> => value =>
		thunks.some(thunk => thunk(value));

	/**
	 * Whether all of the thunks over the input returns `true`
	 * @arg thunks Thunks to pipe together
	 */
	export const all = <T>(...thunks: Thunk<T>[]): Thunk<T> => not(any(...thunks.map(not)));
}
