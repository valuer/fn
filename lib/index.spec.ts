import "mocha";
import { assert } from "chai";
import { fn } from "./index";

const isNumber = (v: any) => typeof v === "number";
const isString = (v: any) => typeof v === "string";
const is42 = (value: number) => value === 42;
const is17 = (value: number) => value === 17;

describe('fn.not', () => {
	it("should negate provided thunk", () => {
		assert(fn.not(is42)(17));
		assert(fn.not(is17)(42));
	});
});

describe('fn.any', () => {
	it("should verify that any thunk applied to the value returns `true`", () => {
		assert(fn.any(isNumber, is17)(42));
		assert(fn.any(isString, is17)(17));
		assert(fn.any(isString, is17, is42)(<any> "some text"));
	});
});

describe('fn.all', () => {
	it("should verify that all thunks applied to the value return `true`", () => {
		assert(fn.all(isNumber, is17)(17));
		assert(fn.all(isNumber, is42)(42));

		assert.isFalse(fn.all(isNumber, isString, is17, is42)(<any> "some text"));
	});
});
